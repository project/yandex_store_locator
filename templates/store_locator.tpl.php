<?php

/**
 * @file
 * The Template file, which handles the Yandex Store Locator display.
 */

$map_id = drupal_html_id('bh-sl-map-container');
?>
<div class="bh-sl-container">
    <div id="<?php print $map_id; ?>" class="bh-sl-map-container">
        <div class="bh-sl-form-container banner">
            <form id="bh-sl-user-location" method="post" action="#">
                <div class="form-input-div">
                    <div class="form-input">
                        <input type="text" placeholder="City/ State/ Province" id="bh-sl-address"
                            name="bh-sl-address" />
                    </div>
                    <div class="form-input">
                        <select id="bh-sl-maxdistance" name="bh-sl-maxdistance">
                            <option value="1">1 kilometre</option>
                            <option value="3" selected="selected">3 kilometres</option>
                            <option value="5">5 kilometres</option>
                            <option value="10">10 kilometres</option>
                            <option value="25">25 kilometres</option>
                            <option value="50">50 kilometres</option>
                            <option value="100">100 kilometres</option>
                        </select>
                    </div>
                    <div class="form-input">
                        <select id="bh-sl-brands" name="bh-sl-brands">
                            <option value="0">All Categories</option>
                            <option value="cat-A">Category A</option>
                            <option value="cat-B">Category B</option>
                            <option value="cat-C">Category C</option>
                        </select>
                    </div>
                    <div class="form-input2">
                        <button id="bh-sl-submit" type="submit">Go</button>
                    </div>
                </div>

            </form>
            <div class="bh-sl-message msgstores"></div>
        </div>
        <div id="bh-sl-map" class="bh-sl-map"></div>
        <div class="bh-sl-detail"></div>
        <div class="bh-sl-directioninfo">
            <a class="back_to_list" id="direction-back">Back to Detail</a>
            <ul class="direction-path"></ul>
        </div>
        <div class="storeScrollbar bh-sl-loc-list content fluid light">
            <ul id="list" class="list"></ul>
        </div>
    </div>

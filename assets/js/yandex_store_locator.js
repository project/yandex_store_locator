/**
 * @file
 * The javascript File, which controls the Yandex Map API loading.
 */

(function ($) {
  'use strict';
  Drupal.behaviors.yandex_store_locator = {
    attach: function (context, settings) {
      var coordinates = Drupal.settings.ydx.coordinates;
      var currentGeo = [coordinates.lat, coordinates.lng];
      var EarthRadius = 6367.0;
      var listTemplatePath = Drupal.settings.ydx.module_path + '/templates/list-display.html';
      var detailTemplatePath = Drupal.settings.ydx.module_path + '/templates/detail-display.html';
      var map;
      var ymapGeocoder = [];
      var searchcoord = [];
      var storeList = [];
      var ymapclusterer = [];
      var storeGeoObject = [];
      var routeGeoObject;
      var templateData = [];
      var listTemplate;
      var listOutput = '#list';
      var detailOutput = '.bh-sl-detail';
      var templateError = 'Cannot load the template from the path' + listTemplatePath;
      var markerDefaultClass = 'ymaps-2-1-39-placemark-overlay ymaps-2-1-39-user-selection-none';
      var markerAnimate = ' bounce animated';
      var markerOption = {
        iconLayout: 'default#image',
        iconImageHref: 'sites/all/modules/yandex_store_locator/assets/img/marker.png',
        iconImageSize: [32, 32],
        iconOffset: [0, 0]
      };
      var clusterIconss = [{
        href: 'sites/all/modules/yandex_store_locator/assets/img/picto.png',
        size: [64, 64],
        offset: [-30, -60]
      }];
      storeList = Drupal.settings.ydx.json_data;

      /**
       * Converting the Coordinate Radiance values.
       *
       * @param {Number} v Coordinate for Conversion.
       *
       * @return {Number} Radian value of Coordinate.
       */
      function _geocodecalctoradian(v) {
        return v * (Math.PI / 180);
      }

      /**
       * Difference between Coordinates.
       *
       * @param {Number} v1 First Coordinate.
       * @param {Number} v2 Second Coordinate.
       *
       * @return {Number} Difference between Number.
       */
      function _geocodecalcdiffs(v1, v2) {
        return _geocodecalctoradian(v2) - _geocodecalctoradian(v1);
      }

      /**
       * Distance calculations between Coordinates.
       *
       * @param {Number} lat1 Latitude of First Coordinate.
       * @param {Number} lng1 Longitude of Frist Coordinate.
       * @param {Number} lat2 Latitude of Second Coordinate.
       * @param {Number} lng2 Longitude of Second Coordinate.
       * @param {Number} radius Radius to Search.
       *
       * @return {Number} Radius of Coordinates.
       */
      function _geocodecalcdiffradian(lat1, lng1, lat2, lng2, radius) {
        return radius * 2 * Math.asin(Math.min(1, Math.sqrt((Math.pow(Math.sin((_geocodecalcdiffs(lat1, lat2)) / 2.0
            ), 2.0) + Math.cos(_geocodecalctoradian(lat1)) * Math.cos(_geocodecalctoradian(lat2)) * Math.pow(
            Math.sin((_geocodecalcdiffs(lng1, lng2)) / 2.0), 2.0)))));
      }

      /**
       * Rounding the Decimal.
       *
       * @param {Number} num Value to be power of.
       * @param {Number} dec Power of the Value.
       *
       * @return {Number}.
       */
      function _roundnumber(num, dec) {
        return Math.round(num * Math.pow(10, dec)) / Math.pow(10, dec);
      }

      /**
       * Sort by Alphabet Ascending order, default Sorted by Title of Stores.
       *
       * @param {Object[]} locationsarray Data to be sorted.
       */
      function _sortalphabet(locationsarray) {
        locationsarray.sort(function (a, b) {
          if (a.title < b.title) {
            return -1;
          }
          else if (a.title > b.title) {
            return 1;
          }
          else {
            return 0;
          }
        });
      }

      /**
       * Render the template for Listing and Detail Store, through Handlebar.js.
       *
       * @param {string} templatePath Path of Template.
       * @param {Object} listingData Data passed to Template.
       * @param {string} output Id on which the Template has to updated.
       */
      function _loadtemplate(templatePath, listingData, output) {
        $.when($.get(templatePath, function (template) {
          var source = template;
          listTemplate = Handlebars.compile(source);
        })).then(function () {
          var listHtml = listTemplate(listingData);
          $('' + output).html(listHtml);
        }, function () {
          // JSON/XML templates not loaded.
          $('' + output).append(templateError);
        });
      }

      /**
       * Route Directions for the Coordinates.
       *
       * @param {Object} directionData Destination Coordinate.
       * @param {Object} currentGeos Source Coordinate.
       */
      function _routedirections(directionData, currentGeos) {
        new ymaps.route([{
          type: 'viaPoint',
          point: directionData.coordinates,
          iconLayout: 'default#image',
          iconImageHref: 'sites/all/modules/yandex_store_locator/assets/img/marker.png',
          iconImageSize: [32, 32],
          iconOffset: [0, 0]
        }, {
          type: 'viaPoint',
          point: currentGeos
        }], {
          mapStateAutoApply: true,
          multiRoute: false,
          routingMode: 'Masstransit'
        }).then(function (route) {
          routeGeoObject = route;
          map.geoObjects.add(route);
          var points = route.getWayPoints();
          points.options.set(markerOption);
          var moveList = 'Start ,</br>';
          var way;
          var segments;
          // Getting an array of paths.
          for (var i = 0; i < route.getPaths().getLength(); i++) {
            way = route.getPaths().get(i);
            segments = way.getSegments();
            for (var j = 0; j < segments.length; j++) {
              var street = segments[j].getStreet();
              moveList += ('<li> Going ' + segments[j].getHumanAction() + (street ? ' on ' + street : '') +
                ', pass' + segments[j].getLength() + ' m.,</li>');
            }
          }
          moveList += '<li>We Stop.</li>';
          $('.bh-sl-detail').hide();
          $('.direction-path').html(moveList);
          $('.bh-sl-directioninfo').show();
        }, function (error) {
          var moveListNotFound = 'No direction were found';
          $('.bh-sl-detail').hide();
          $('.direction-path').html(moveListNotFound);
          $('.bh-sl-directioninfo').show();
        });
      }

      /**
       * To get the Geo Location of the User.
       *
       * @param {Object} directionDataTemp Destination Coordinate.
       */
      function getgeolocation(directionDataTemp) {
        navigator.geolocation.getCurrentPosition(function (position) {
          var currentGeon = [position.coords.latitude, position.coords.longitude];
          _routedirections(directionDataTemp, currentGeon);
        });
      }

      // Load Yandex Map when ready.
      ymaps.ready(function () {
        map = new ymaps.Map('bh-sl-map', {
          center: currentGeo,
          zoom: 4
        });
        map.controls.remove('mapTools').remove('searchControl').remove('trafficControl').remove('typeSelector');
        _sortalphabet(storeList);
        // Add additional informations need for the Data to be displayed in the Template.
        for (var j = 0; j < storeList.length; ++j) {
          storeList[j].markerid = j;
          storeList[j].coordinates = [storeList[j].lat, storeList[j].lng];
          storeList[j].distance = _roundnumber(_geocodecalcdiffradian(storeList[j].lat, storeList[j].lng,
            currentGeo[0], currentGeo[1], EarthRadius), 2);
          var sBrands = storeList[j].category.split(',');
          storeList[j].carriesArray = sBrands;
          templateData.push(storeList[j]);
        }
        // Create Yandex Geoobjects with Data.
        for (var i = 0; i < templateData.length; i++) {
          storeGeoObject[i] = new ymaps.GeoObject({
            geometry: {
              type: 'Point',
              coordinates: templateData[i].coordinates,
              dmarkerid: i
            }
          });
          storeGeoObject[i].dmarker = i;
          storeGeoObject[i].options.set(markerOption);
        }
        var listingDataInitial = {
          location: templateData
        };
        _loadtemplate(listTemplatePath, listingDataInitial, listOutput);
        $('.bh-sl-message').html(storeGeoObject.length + ' Stores ');
        // Create Yandex Cluster object with the Geoobjects.
        ymapclusterer = new ymaps.Clusterer({
          clusterIcons: clusterIconss,
          groupByCoordinates: false,
          minClusterSize: 2,
          margin: 100
        });
        ymapclusterer.add(storeGeoObject);
        map.geoObjects.add(ymapclusterer);

        // Click Event of GeoObjects (Marker,Cluster).
        map.geoObjects.events.add('click', function (e) {
          var object = e.get('target');
          if (!object.hasOwnProperty('_clusterBounds')) {
            var objmarker = object.dmarker;
            map.setCenter(object.geometry.getCoordinates(), 12);
            object.getOverlay()._value._view._element.className += ' bounce animated';
            $('#list li').removeClass('highlight');
            $('#list li[data-markerid=' + objmarker + ']').addClass('highlight');
            $('#list').scrollTop($('#list li[data-markerid=' + objmarker + ']'), {
              duration: 'slow',
              offsetTop: '50'
            });
            $('.bh-sl-loc-list').show();
            $('.bh-sl-detail').hide();
            $('.bh-sl-directioninfo').hide();
          }
          else {
            $('#list li').removeClass('highlight');
          }
        });

      });

      $(document).ready(function () {

        $('body').on('click', '.back_to_list a', function () {
          map.geoObjects.remove(routeGeoObject);
          $('.bh-sl-detail').hide();
          $('.bh-sl-directioninfo').hide();
          $('.bh-sl-loc-list').show();
          map.setCenter(currentGeo, 4);
        });

        $('body').on('click', '.bh-sl-loc-list li', function () {
          var index = $(this).attr('data-markerid');
          map.setCenter(templateData[index].coordinates, 13);
          $('.bh-sl-loc-list').hide();
          $('.bh-sl-directioninfo').hide();
          if (storeGeoObject[index].getOverlay()._status === 2) {
            storeGeoObject[index].getOverlay()._value._view._element.className += markerAnimate;
          }
          var detailData = {
            storedetail: templateData[index]
          };
          _loadtemplate(detailTemplatePath, detailData, detailOutput);
          $('.bh-sl-detail').show();

        });

        $('body').on('mouseenter ', '.bh-sl-loc-list li', function () {
          var index = $(this).attr('data-markerid');
          if (storeGeoObject[index].getOverlay()._status === 2) {
            markerDefaultClass = storeGeoObject[index].getOverlay()._value._view._element.className;
            storeGeoObject[index].getOverlay()._value._view._element.className += markerAnimate;
          }
        });
        $('body').on('mouseleave ', '.bh-sl-loc-list li', function () {
          var index = $(this).attr('data-markerid');
          if (storeGeoObject[index].getOverlay()._status === 2) {
            storeGeoObject[index].getOverlay()._value._view._element.className = markerDefaultClass.replace(markerAnimate, '');
          }
        });

        $('body').on('click', '#direction-back', function () {
          map.geoObjects.remove(routeGeoObject);
          $('.bh-sl-detail').show();
          $('.bh-sl-directioninfo').hide();
        });

        $('body').on('click', '.loc-directions', function () {
          var index = $(this).children('a#loc-getdirection').attr('data-markerid');
          var directionData = templateData[index];
          if (navigator.geolocation) {
            getgeolocation(directionData);
          }
          else {
            _routedirections(directionData, currentGeo);
          }
        });

        $('body').on('click', '#bh-sl-submit', function (e) {
          e.preventDefault();
          $('.bh-sl-detail').hide();
          $('.bh-sl-loc-list').show();
          var searchData = $('#bh-sl-address').val();
          var maxdistance = $('#bh-sl-maxdistance').val();
          var brands = $('#bh-sl-brands').val();
          ymapGeocoder = ymaps.geocode(searchData);
          var searachCount = 0;
          ymapGeocoder.then(function (res) {
            if (res.geoObjects.get(0)) {
              searchcoord = res.geoObjects.get(0).geometry.getCoordinates();
            }
            else {
              searchcoord = [-1, -1];
            }
            if (searchcoord[0] !== -1) {
              var filterData = [];
              _sortalphabet(storeList);
              for (var i = 0; i < storeList.length; i++) {
                var serachBrandsTrim = storeList[i].category.split(',');
                var distance = _geocodecalcdiffradian(
                    storeList[i].coordinates[0],
                    storeList[i].coordinates[1],
                    searchcoord[0],
                    searchcoord[1],
                    EarthRadius
                );
                storeList[i].distance = _roundnumber(distance, 2);
                if (typeof maxdistance !== 'undefined' && maxdistance !== null) {
                  if (distance <= maxdistance) {
                    if (brands === '0') {
                      storeList[i].markerid = searachCount;
                      filterData.push(storeList[i]);
                      searachCount++;
                    }
                    else if (serachBrandsTrim.indexOf(brands) > -1) {
                      storeList[i].markerid = searachCount;
                      filterData.push(storeList[i]);
                      searachCount++;
                    }
                  }
                }
                else {
                  filterData.push(storeList[i]);
                }
              }
              templateData = filterData;
              var listingDataSearch = {
                location: templateData
              };
              var centerZoom = 6;
              if (templateData.length > 0) {
                $('.bh-sl-message').addClass('msgstores');
                $('.bh-sl-message').html(templateData.length + ' Stores ');
                centerZoom = 12 - templateData.length;
                if (templateData.length >= 5) {
                  centerZoom = 6;
                }
                map.setCenter([searchcoord[0], searchcoord[1]], centerZoom);
                map.geoObjects.removeAll();
                if (templateData) {
                  storeGeoObject = [];
                  ymapclusterer = new ymaps.Clusterer({
                    clusterIcons: clusterIconss,
                    groupByCoordinates: false,
                    minClusterSize: 2,
                    margin: 100
                  });
                  for (var j = 0; j < templateData.length; j++) {
                    storeGeoObject[j] = new ymaps.GeoObject({
                      geometry: {
                        type: 'Point',
                        coordinates: templateData[j].coordinates
                      }
                    });
                    storeGeoObject[j].options.set(markerOption);

                  }
                  ymapclusterer.add(storeGeoObject);
                  map.geoObjects.add(ymapclusterer);
                  _loadtemplate(listTemplatePath, listingDataSearch, listOutput);

                }
              }
              else {
                _sortalphabet(storeList);
                templateData = storeList;
                $('.bh-sl-message').removeClass('msgstores');
                $('.bh-sl-message').html('No Stores are found');
              }

            }
            else {
              $('.bh-sl-message').removeClass('msgstores');
              $('.bh-sl-message').html('Please provide a vaild address');
            }
          });

        });
      });

    }
  };
})(jQuery);

About Yandex Store Locator for Drupal
-------------------------
This module allows Drupal to integrate Yandex Map and manage the Stores
by locating the Store through Map.

Configure Module
-------------------------
Download the Animate CSS from the URL https://daneden.github.io/animate.css/ .
Place the CSS in the sites/all/libraries/animate folder.

Download the Handlebars JS.
Place the JS in the sites/all/libraries/handlerbar folder.

Sample Test Case
-------------------------
Filter is done is based on the Location (similar the Google Store Locator)
entered by the User which is decoded as Coordinates (by Yandex API) and
compared with Store's Coordinates to display the Results.
Search with following the location and change the 'Kilometer' level find the
Results changing. 
1. western river port
2. filyovsky Park
Store data provided is Sample Content
